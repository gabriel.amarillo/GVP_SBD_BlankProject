
function getNavegacion(){
	
	var appCallData = getAppVar('AppCallData');
	
	if (appCallData.gvpMenu == undefined)
	{
		appCallData.gvpMenu =1;
		return 1;
	}
	else{
		
		appCallData.gvpMenu = appCallData.gvpMenu + 1;
		
		setAppVar('AppCallData',appCallData);
		return appCallData.gvpMenu;
		
	}
	
}


function getBaseUri(){

var url = session.connection.protocol.sip.requesturi.voicexml;
var pathArray = url.split( '/' )
return pathArray[0]+'/' + pathArray[1] + '/' + pathArray[2];

}
	

function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}
function setAppVar(appVarKey, appVarVal){
 return session.com.genesyslab.userdata[appVarKey] = appVarVal;
}

function getAppVar(appVarKey){
 if (session.com.genesyslab.userdata[appVarKey] == undefined) {
  return "";
 } else {
  return session.com.genesyslab.userdata[appVarKey];
 }
}

